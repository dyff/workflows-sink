# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

import importlib

import pytest


@pytest.mark.parametrize(
    "module_name",
    [
        "workflows_sink.config",
        "workflows_sink.main",
        "workflows_sink.typing",
    ],
)
def test_import_module(module_name):
    importlib.import_module(module_name)
