# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

"""Configuration options for Dyff Platform components.

Our approach to configuration:

  1. Config options are specified with Pydantic models where all fields are
     optional.
  2. Config values are provided to the component through env variables.
  3. Env variables are named like ``DYFF_OPTION`` or
     ``DYFF_SUBCONFIG__OPTION``. The first value is available at
     ``config.option``, and the second is available at
     ``config.subconfig.option``. The nested config value requires creating
     a ``pydantic.BaseModel`` to hold the ``.subconfig`` options.
  4. When running the component in k8s, mount configuration values as
     environment variables. Mount only the subsets of the configuration that
     are required for the component. Both ``ConfigMap`` and ``Secret``
     resources can be mounted.

"""

import os
from typing import List, Optional

from dyff.storage.config import StorageConfig
from pydantic import BaseModel, BaseSettings, Field, SecretStr
from pydantic.fields import Undefined


class KafkaConfigConfig(BaseModel):
    bootstrap_servers: str = Field(
        default="kafka.kafka.svc.cluster.local",
        description="The address to contact when establishing a connection to Kafka.",
        examples=[
            "kafka.kafka.svc.cluster.local",
            "kafka.kafka.svc.cluster.local:9093",
        ],
    )

    client_id: Optional[str] = Field(description="client.id")

    compression_type: str = "zstd"

    group_id: Optional[str] = Field(description="Consumer group.id")

    def get_producer_config(self, keys: Optional[List[str]] = None):
        if keys is None:
            keys = ["bootstrap.servers", "compression.type"]
        return {key: self.get_by_kafka_key(key) for key in keys}

    def get_by_kafka_key(self, key: str):
        field_name = key.replace(".", "_")
        return getattr(self, field_name)


class KafkaTopicsConfig(BaseModel):
    commands: str = None
    workflows_events: str = Field(
        default="dyff.workflows.events",
        examples=["test.workflows.events"],
    )
    workflows_state: str = Field(
        default="dyff.workflows.state",
        examples=["test.workflows.state"],
    )


class KafkaConfig(BaseModel):
    config: KafkaConfigConfig = Field(default_factory=KafkaConfigConfig)
    topics: KafkaTopicsConfig = Field(default_factory=KafkaTopicsConfig)


def mongodb_connection_string_field(default: Optional[str] = None) -> SecretStr:
    if default is None:
        default = SecretStr(
            "mongodb://localhost:27017/&ssl=false",
        )
    return Field(
        description=(
            """Set the MongoDB connection string, following this pattern::

    mongodb+srv://[username:password@]host[/[defaultauthdb][?options]]

For more info, see the `MongoDB manual
<https://www.mongodb.com/docs/manual/reference/connection-string/>`_.
"""
        ),
        default=default,
        examples=[
            "mongodb+srv://USER:PASS@dyff-datastore-rs0.mongodb.svc.cluster.local/workflows?replicaSet=rs0&ssl=false&authSource=users",
        ],
    )


def mongodb_database_field(default=Undefined) -> str:
    return Field(
        description=("""Name of the MongoDB database to connect to."""),
        default=default,
        examples=[
            "accounts",
            "workflows",
        ],
    )


class MongoDBSinkConfig(BaseModel):
    connection_string: SecretStr = mongodb_connection_string_field()
    database: str = mongodb_database_field(default="workflows")


class S3SinkConfig(BaseModel):
    storage_path: Optional[str] = Field(
        description="Storage path in s3, e.g. 's3://some/path'", default=None
    )


class StorageConfigV2(BaseModel):
    url: str = Field(
        description=(
            """File storage is provided by the smart_open_ package, and any
supported URL format may be used. Dyff is currently tested with Google Cloud
Storage and MinIO.

Additional configuration may be required. See the `smart_open documentation`__
for more information.

.. _smart_open: https://pypi.org/project/smart-open/

__ smart_open_
"""
        ),
        default="s3://dyff",
        examples=[
            "/path/to/dyff",
            "gs://dyff",
        ],
    )


class WorkflowsSinkConfig(BaseModel):
    sink_kind: Optional[str] = Field(default=None)
    mongodb: MongoDBSinkConfig = Field(default_factory=MongoDBSinkConfig)
    s3: S3SinkConfig = Field(default_factory=S3SinkConfig)


class SafetyCasesConfig(BaseModel):
    storage: StorageConfigV2 = Field(
        default_factory=lambda: StorageConfigV2(
            url="gs://alignmentlabs-reports-c1caea9ce5861c9e"
        )
    )


class ResourcesConfig(BaseModel):
    safetycases: SafetyCasesConfig = Field(default_factory=SafetyCasesConfig)


class DyffConfig(BaseSettings):
    workflows_sink: WorkflowsSinkConfig = Field(default_factory=WorkflowsSinkConfig)
    kafka: KafkaConfig = Field(default_factory=KafkaConfig)
    storage: StorageConfig = Field(default_factory=StorageConfig)
    resources: ResourcesConfig = Field(default_factory=ResourcesConfig)

    class Config:
        env_file = os.environ.get("DYFF_DOTENV_FILE", ".env")
        # Env variables start with 'DYFF_'
        env_prefix = "dyff_"
        # Env var like 'DYFF_KAFKA__FOO' will be stored in 'kafka.foo'
        env_nested_delimiter = "__"


config = DyffConfig()
