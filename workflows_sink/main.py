# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"
import json

import absl.flags
from absl import logging
from confluent_kafka import Consumer, KafkaError, KafkaException, Message
from dyff.schema import ids

from .config import config
from .typing import YAMLObject

FLAGS = absl.flags.FLAGS

absl.flags.DEFINE_bool(
    "from_beginning",
    False,
    "Consume the topic from the beginning (i.e., reset the commit offset)",
)


def serialize_id(id: str) -> bytes:
    return id.encode("utf-8")


def deserialize_id(id: bytes) -> str:
    return id.decode("utf-8")


def deserialize_value(value: bytes) -> YAMLObject:
    return json.loads(value.decode("utf-8"))


class WorkflowsSink:
    def __init__(self):
        if config.workflows_sink.sink_kind == "mongodb":
            from .sinks import mongodb

            self.sink = mongodb.MongoDBSink()
        elif config.workflows_sink.sink_kind == "s3":
            from .sinks import s3

            self.sink = s3.S3Sink()
        else:
            raise ValueError(f"unknown sink_kind: {config.workflows_sink.sink_kind}")

    def process_message(self, msg: Message):
        logging.info(f"{msg.key()}: {msg.value()}")

        key = deserialize_id(msg.key())
        if key == ids.null_id():
            logging.debug(f"skipping null ID: {msg}")
            return

        entity = deserialize_value(msg.value()) if msg.value() is not None else None

        if entity is not None:
            entity_id = entity.get("id")
            if entity_id is None:
                logging.error(f"entity.id is None; skipping message")
                return
            if key != entity_id:
                logging.error(f"entity.id {entity_id} != key {key}; skipping message")
                return
            if entity_id == ids.null_id():
                logging.debug(f"skipping null ID: {msg}")
                return

        return self.sink.process_message(msg, key, entity)

    def on_commit(self, error, partitions):
        if error:
            logging.error(error)
        else:
            logging.debug(f"committed partition offsets: {partitions}")

    def run(self) -> None:
        consumer_config = {
            "bootstrap.servers": config.kafka.config.bootstrap_servers,
            "group.id": config.kafka.config.group_id,  # "dyff.system.mongodb.sink",
            "auto.offset.reset": "earliest",
            "enable.auto.commit": False,
            "on_commit": self.on_commit,
        }
        consumer = Consumer(consumer_config)

        def on_assign(consumer, partitions):
            # If reading from beginning, reset all partition offsets
            if FLAGS.from_beginning:
                for partition in partitions:
                    # Note: -2 is the Kafka special value meaning "from beginning"
                    partition.offset = -2
            # This is what the default on_assign does
            consumer.assign(partitions)

        try:
            consumer.subscribe(
                [config.kafka.topics.workflows_state], on_assign=on_assign
            )
            while True:
                msg = consumer.poll(timeout=1.0)
                if msg is None:
                    continue

                if msg.error():
                    if msg.error().code() == KafkaError._PARTITION_EOF:
                        logging.warning(
                            f"{msg.topic()}:{msg.partition()} EOF at offset {msg.offset()}"
                        )
                    else:
                        raise KafkaException(msg.error())
                else:
                    # consumer.commit() after processing => at-least-once processing
                    self.process_message(msg)
                    consumer.commit(asynchronous=True)
        finally:
            consumer.close()
