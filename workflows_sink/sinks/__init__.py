# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"
from typing import Protocol

from confluent_kafka import Message

from ..typing import YAMLObject


class Sink(Protocol):
    def process_message(self, msg: Message, entity: YAMLObject) -> None:
        pass
