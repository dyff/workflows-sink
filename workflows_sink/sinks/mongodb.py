# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"
import json
from typing import Optional

import pymongo
import pymongo.errors
import pymongo.write_concern
from absl import logging
from confluent_kafka import Message
from dyff import storage
from dyff.schema import ids
from dyff.schema.platform import Entities, Resources, Score, is_status_success

from ..config import config
from ..typing import YAMLObject


def _labels_object_to_array(obj: dict) -> list:
    # None value => Delete the label/annotation
    return [{"key": k, "value": v} for k, v in obj.items() if v is not None]


def _to_mongodb_entity(entity: YAMLObject) -> YAMLObject:
    d = dict(entity)
    d["_id"] = d.pop("id")

    # Transform dicts with uncontrolled keys to a list of (k, v) pairs to avoid
    # problems with special characters in keys

    if (labels := d.get("labels")) is not None:
        d["labels"] = _labels_object_to_array(labels)  # type: ignore
    # if "annotations" in d:
    #     d["annotations"] = object_to_array(d["annotations"])
    if d["kind"] == "Family":
        if (members := d.get("members")) is not None:
            d["members"] = _labels_object_to_array(members)  # type: ignore

    return d


class MongoDBSink:
    def __init__(self):
        connection_string = config.workflows_sink.mongodb.connection_string
        self._client = pymongo.MongoClient(connection_string.get_secret_value())
        self._workflows_db = self._client.get_database(
            config.workflows_sink.mongodb.database,
            write_concern=pymongo.write_concern.WriteConcern("majority", wtimeout=5000),
        )

    def _forget_entity(self, msg: Message, key: str) -> None:
        logging.debug(
            f"{msg.key().decode()}: op=forget, offset={msg.offset()}, partition={msg.partition()}"
        )

        # FIXME: (DYFF-582) At this point, all we have is the entity ID, so we
        # don't know what kind it is and all we can do is look for it in every
        # collection. I think the best solution is actually to move all
        # the entities to a single collection.
        collection_names = [
            resource.value
            # All top-level entities
            for resource in (
                Resources.Dataset,
                Resources.Evaluation,
                Resources.Family,
                Resources.InferenceService,
                Resources.InferenceSession,
                Resources.Measurement,
                Resources.Method,
                Resources.Model,
                Resources.Module,
                Resources.Report,
                Resources.Revision,
                Resources.SafetyCase,
                Resources.UseCase,
            )
        ]
        for kind in collection_names:
            result = self._workflows_db[kind].delete_one({"_id": key})
            if result.deleted_count == 1:
                break
        else:
            logging.warning(f"tombstone: entity {key} not found in database")
            return

        # Documentation is stored under the same ID as the entity
        self._workflows_db[Resources.Documentation].delete_one({"_id": key})

        if kind == Resources.SafetyCase:
            # Scores reference the workflow that created them in .analysis
            self._workflows_db[Resources.Score].delete_many({"analysis": key})

    def _update_entity(self, msg: Message, key: str, entity: YAMLObject) -> None:
        logging.debug(
            f"{msg.key().decode()}: op=update, offset={msg.offset()}, partition={msg.partition()}"
        )

        try:
            entity_id = entity["id"]
            entity_kind = entity["kind"]
            mongodb_entity = _to_mongodb_entity(entity)
            resource = Resources.for_kind(Entities(entity_kind))
        except KeyError:
            logging.exception(
                f"entity missing required field; skipping message; in:\n{msg}"
            )
            return
        except ValueError:
            logging.exception(
                f"unexpected unhandled kind; skipping message; {entity_kind} in:\n{msg}"
            )
            return

        logging.info(f"update: {entity_id}")
        self._workflows_db[resource].update_one(
            {"_id": entity_id}, {"$set": mongodb_entity}, upsert=True
        )

        # Insert Scores data into DB for applicable workflows
        if entity_kind == Entities.SafetyCase.value:
            status = entity["status"]
            if isinstance(status, str) and is_status_success(status):
                storage_root = config.resources.safetycases.storage.url
                storage_path = f"{storage_root}/{entity_id}/.dyff/scores.json"
                try:
                    scores_bytes = storage.get_object(storage_path)
                except Exception:
                    logging.warning(f"no scores.json found for safetycases/{entity_id}")
                    return

                scores_data = json.load(scores_bytes)
                scores: list = []
                for score_data in scores_data["scores"]:
                    score_data["kind"] = Entities.Score.value
                    score_data["id"] = ids.namespaced_id(
                        score_data["analysis"], score_data["name"]
                    )
                    scores.append(
                        _to_mongodb_entity(Score.parse_obj(score_data).model_dump())
                    )

                try:
                    # ordered=False so that all inserts are attempted even if
                    # some of them fail
                    self._workflows_db["scores"].insert_many(scores, ordered=False)
                except pymongo.errors.BulkWriteError as ex:
                    # E11000 duplicate key error
                    # This is expected and harmless because of at-least-once
                    # semantics.
                    # Note that the top-level ex.code is *not* 11000 in this
                    # case. Not clear what ex.code actually represents here.
                    for write_error in ex.details["writeErrors"]:
                        if write_error["code"] != 11000:
                            raise

    def process_message(
        self, msg: Message, key: str, entity: Optional[YAMLObject]
    ) -> None:
        if entity is None:
            return self._forget_entity(msg, key)
        else:
            return self._update_entity(msg, key, entity)
