# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

# mypy: disable-error-code="import-untyped"
import base64
import json
from typing import Optional

from absl import logging
from confluent_kafka import Message
from dyff import storage

from ..config import config
from ..typing import YAMLObject


def encode_bytes(b: bytes) -> str:
    return base64.b64encode(b).decode("utf-8")


def deserialize_id(id: bytes) -> str:
    return id.decode("utf-8")


class S3Sink:
    def __init__(self):
        if config.workflows_sink.s3.storage_path is None:
            raise ValueError("storage_path is None")
        # Just in case someone gives a path like 's3://some/path/'
        self.storage_path = config.workflows_sink.s3.storage_path.rstrip("/")

    def _object_path(self, msg: Message, key: str) -> str:
        return f"{self.storage_path}/{msg.topic()}/{key}"

    def _forget_entity(self, msg: Message, key: str) -> None:
        logging.debug(
            f"{msg.key().decode()}: op=forget, offset={msg.offset()}, partition={msg.partition()}"
        )

        storage.delete_object(self._object_path(msg, key))

    def _update_entity(self, msg: Message, key: str) -> None:
        logging.debug(
            f"{msg.key().decode()}: op=update, offset={msg.offset()}, partition={msg.partition()}"
        )

        # We treat everything generically as 'bytes' so that if we ever end
        # up backing up other Kafka messages, we can use uniform decoding logic
        record = {
            "kafka.Message": {
                "topic": msg.topic(),
                "partition": msg.partition(),
                "offset": msg.offset(),
                "headers": (
                    msg.headers() and [(k, encode_bytes(v)) for k, v in msg.headers()]
                ),
                "key": encode_bytes(msg.key()),
                "value": encode_bytes(msg.value()),
            }
        }

        storage.put_object(
            json.dumps(record).encode("utf-8"), self._object_path(msg, key)
        )

    def process_message(
        self, msg: Message, key: str, entity: Optional[YAMLObject]
    ) -> None:
        if entity is None:
            return self._forget_entity(msg, key)
        else:
            return self._update_entity(msg, key)
